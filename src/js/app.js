var css = require('../css/app.scss');
var THREE = require('three');
var OrbitControls = require('three-orbit-controls')(THREE);
var fbx = require('three-fbx-loader')(THREE);


var renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

// Scene
var scene = new THREE.Scene();
scene.background = new THREE.Color( 0xffffff );

// Camera
var  camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 1000 );

// Controls
var controls = new OrbitControls( camera );
camera.position.set( 0, 1, 2 );
controls.update();

// Model
var loader = new THREE.FBXLoader();
loader.load('src/js/poro.fbx', handle_load);

function handle_load(geometry, materials){
  var mesh = new THREE.Mesh(geometry, materials);
  scene.add(mesh);
  mesh.position.z = -10;
}

// Render loop
var animate = function () {
  requestAnimationFrame( animate );
  controls.update();

  renderer.render(scene, camera);
};

animate();