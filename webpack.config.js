var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var path = require("path");

module.exports = {
  entry: {
    app: './src/js/app.js',
    contact: './src/js/contact.js'
  },
  output: {
    path: path.resolve(__dirname, '/dist'),
    filename: '[name].bundle.js'
  },
  module:{
    rules: [
      { 
        test: /\.scss$/, 
        use: ExtractTextPlugin.extract({ 
          fallback: 'style-loader', 
          use: [ 'css-loader', 'sass-loader' ],
          publicPath: '/dist' 
        })
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      },
      {
        test: /\.(jpe?g|png|gif|svg|fbx)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].[ext]',
              publicPath: 'assets/'
            }  
          }
        ]
      },

    ]
  },
  devServer:{
    contentBase: path.join(__dirname, "dist"),
    compress: true, 
    stats: "errors-only"
  },
  plugins: [

    new HtmlWebpackPlugin({
      title:'Learning ThreeJS',
      /*minify: {
        collapseWhitespace: true

      },*/
      excludeChunks: ['contact'],
      hash: true,
      template: './src/index.html',
    }),
    new HtmlWebpackPlugin({
      title:'Contact Page',
      chunks: ['contact'],
      hash: true,
      filename: 'contact.html',
      template: './src/contact.html'
    }),
    new ExtractTextPlugin({
      filename: 'app.css',
      disable: true,
      allChunks: true
    })
  ]
}